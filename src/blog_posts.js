export default [
	{
		id: '1',
		title: 'TOP 10 TRENDS IN 2022 FOR JEWELLERY IN AUSTRALIA',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-1.jpg')
	},
	{
		id: '2',
		title: 'HOW TO PROPERLY CLEAN SILVER JEWELLERY',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-2.jpg')
	},
	{
		id: '3',
		title: '5 THINGS TO LOOK FOR WHEN SHOPPING FOR AUSTRALIAN JEWELLERY',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-3.jpg')
	},
	{
		id: '4',
		title: 'MOST POPULAR JEWELLERY FOR CHRISTMAS 2021',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-4.jpg')
	},
	{
		id: '5',
		title: '10 TIPS FOR SAFELY BUYING JEWELLERY ONLINE IN AUSTRALIA',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-5.jpg')
	},
	{
		id: '6',
		title: '5 TIPS TO FIND THE BEST AUSTRALIAN JEWELLERS',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-6.jpg')
	},
	{
		id: '7',
		title: 'WE NEED TO TALK ABOUT CHRISTMAS 2021',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-7.jpg')
	},
	{
		id: '8',
		title: 'HOW TO CHOOSE THE RIGHT NECKLACE LENGTH',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-8.jpg')
	},
	{
		id: '9',
		title: '15 JEWELLERY STYLE TIPS YOU NEED TO KNOW',
		text: `How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.How do you choose and style jewellery to complement yourself and your wardrobe? And how do you make best use of your jewellery collection without being boring? The answers aren't always obvious. Never fear: we've come up with our 15 top jewellery style tips to help you be your fashionable best.`,
		img: require('./assets/images/blog-img/i-9.jpg')
	},
]